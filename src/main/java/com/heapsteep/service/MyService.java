package com.heapsteep.service;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.heapsteep.model.Person;
import com.heapsteep.repository.MyRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MyService {
	
	Person person;
	
	@Autowired
    MyRepository myRepository;
	
	public Flux<Person> findAll() {
		return myRepository.findAll();
	}
	
	public Mono<Person> save(Person person) {
        person.setId((int)(Math.random() * 100) + 1);
        return myRepository.save(person);
    }
}
