package com.heapsteep.controller;

import com.heapsteep.model.Person;
import com.heapsteep.repository.MyRepository;
import com.heapsteep.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class MyController {

    @Autowired
    MyService myService;

    @GetMapping("/getPersons")
    public Flux<Person> getPersons() {
        return myService.findAll();
    }

    @PostMapping ("/addPerson")
    public Mono<Person> addPerson(@RequestBody Person person){
        return myService.save(person);
    }
}
