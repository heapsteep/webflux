package com.heapsteep.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.annotation.Documented;

@Document(collection="Person_collection")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
	
	@Id
	private int id;
	private String name;
	private double salary;

}
