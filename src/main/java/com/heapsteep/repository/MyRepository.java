package com.heapsteep.repository;

import org.springframework.data.repository.CrudRepository;

import com.heapsteep.model.Person;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

public interface MyRepository extends ReactiveCrudRepository<Person,String> {

}
